#include <string>
#include "AudioFile.h"
#ifndef UNTITLED4_AUDIORENDER_H
#define UNTITLED4_AUDIORENDER_H

#define BIAS 3.2

class AudioRender {
public:
    explicit AudioRender(const std::string& filepath) {
        audioFile.load(filepath);
        numSamples = audioFile.getNumSamplesPerChannel();
        numChannels = audioFile.getNumChannels();
    }
    AudioFile<double> beat_parcing(int bigbuf_samples, int smallbuf_samples) {
        AudioFile<double>::AudioBuffer bigbuffer;
        AudioFile<double>::AudioBuffer smallbuffer;
        AudioFile<double>::AudioBuffer savebuffer;
        bigbuffer.resize (numChannels);
        smallbuffer.resize (numChannels);
        savebuffer.resize (numChannels);
        for (int channel = 0; channel < numChannels; ++channel) {
            for (int i = 0; i < bigbuf_samples-smallbuf_samples; ++i) {
                bigbuffer[channel].push_back(audioFile.samples[channel][i]);
            }
        }
        for (int channel = 0; channel < numChannels; ++channel) {
            for (int i = bigbuf_samples-smallbuf_samples; i < bigbuf_samples; ++i) {
                bigbuffer[channel].push_back(audioFile.samples[channel][i]);
                smallbuffer[channel].push_back(audioFile.samples[channel][i]);
            }
        }
        int k = bigbuf_samples/smallbuf_samples;
        int q = k;
        int count_of_mini_samples = numSamples/smallbuf_samples;
        while (k < count_of_mini_samples) {
            double small_sum = 0;
            double big_sum = 0;
            double big_big_sum = 0;
            double avg_big_sum = 0;
            double disp = 0;
            double avg_disp = 0;
            for (int j = 0; j < q-1; ++j) {
                for (int channel = 0; channel < numChannels; ++channel) {
                    for (int i = j*smallbuf_samples; i < (j+1)*smallbuf_samples; ++i) {
                        big_sum += bigbuffer[channel][i] * bigbuffer[channel][i];
                    }
                }
                big_big_sum += big_sum;
                big_sum = 0;
            }
            big_sum = 0;
            avg_big_sum = big_big_sum / q;
            for (int j = 0; j < q-1; ++j) {
                for (int channel = 0; channel < numChannels; ++channel) {
                    for (int i = j*smallbuf_samples; i < (j+1)*smallbuf_samples; ++i) {
                        big_sum += bigbuffer[channel][i] * bigbuffer[channel][i];
                    }
                }
                disp += (big_sum - avg_big_sum) * (big_sum - avg_big_sum);
                big_sum = 0;
            }
            avg_disp = disp / q;
            big_sum = 0;
            double beat_constant = (-0.0025714 * avg_disp) + BIAS;
            for (int channel = 0; channel < numChannels; ++channel) {
                for (int i = 0; i < bigbuf_samples-smallbuf_samples; ++i) {
                    big_sum += bigbuffer[channel][i] * bigbuffer[channel][i];
                }
            }
            for (int channel = 0; channel < numChannels; ++channel) {
                for (int i = smallbuf_samples; i > 0; --i) {
                    big_sum += bigbuffer[channel][bigbuf_samples - i] * bigbuffer[channel][bigbuf_samples - i];
                    small_sum += bigbuffer[channel][i-1] * bigbuffer[channel][i-1];
                }
            }
            if (small_sum > beat_constant * avg_big_sum) {
                for (int channel = 0; channel < numChannels; ++channel) {
                    for (int i = 0; i < smallbuf_samples; ++i) {
                        savebuffer[channel].push_back(smallbuffer[channel][i]);
                    }
                }
            }
            for (int channel = 0; channel < numChannels; ++channel) {
                bigbuffer[channel].erase(bigbuffer[channel].begin(), bigbuffer[channel].begin() + smallbuf_samples);
                smallbuffer[channel].clear();
            }

            for (int channel = 0; channel < numChannels; ++channel) {
                for (int i = k * smallbuf_samples; i < (k + 1) * smallbuf_samples; ++i) {
                    bigbuffer[channel].push_back(audioFile.samples[channel][i]);
                    smallbuffer[channel].push_back(audioFile.samples[channel][i]);
                }
            }
            ++k;
        }
        AudioFile<double> buf_audio;
        buf_audio.setAudioBuffer (savebuffer);
        return buf_audio;
    }
    int getNumSamplesPerChannel() {
        return audioFile.getNumSamplesPerChannel();
    }
    AudioFile<double>& getAudioFile() {
        return audioFile;
    }
private:
    AudioFile<double> audioFile;
    int numSamples;
    int numChannels;
};

#endif //UNTITLED4_AUDIORENDER_H

