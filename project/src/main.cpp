#include <iostream>
#include <cmath>

#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "AudioFile.h"
#include "AudioRender.h"
#include <chrono>


#define _USE_MATH_DEFINES
#define MULTIPLY 1
#define SMALLBUF 1024 * MULTIPLY
#define TIME_TO_SPAWN 0.02322f * MULTIPLY

int main() {
    int i = 0;
    AudioRender audio("./Cybor.wav");
    AudioFile<double> circles = audio.beat_parcing(44032, SMALLBUF);  // approx 1 sec - big, 0.02323 sec - small
    circles.save("./circles.wav");
    //AudioFile<double> sliders = audio.beat_parcing(44032 * 20, 1024 * 20);  // approx 20 sec - big, 1 sec - small
    //sliders.save("./sliders.wav");
    sf::Music music;
    if (!music.openFromFile("./Cybor.wav"))
        return -1; // error
    music.play();
    auto start = std::chrono::high_resolution_clock::now();
    sf::Clock clock;
    sf::RenderWindow window(sf::VideoMode(800, 600), "OSU!");
    while (window.isOpen()) {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event)) {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
        }
        double sum1 = 0;
        double sum2 = 0;
        while (clock.getElapsedTime().asSeconds() >= TIME_TO_SPAWN && !circles.samples[0].empty()) {
            for (int j = i; j < i + SMALLBUF; ++j) {
                sum2 += (audio.getAudioFile().samples[0][j] * audio.getAudioFile().samples[0][j]);
            }
            for (int j = 0; j < SMALLBUF; ++j) {
                sum1 += (circles.samples[0][j] * circles.samples[0][j]);
            }
            if (std::fabs(sum1 - sum2 ) < 1e-10) {
                circles.samples[0].erase(circles.samples[0].begin(), circles.samples[0].begin() + SMALLBUF);
                window.clear();
                sf::CircleShape shape(50.f);
                shape.setFillColor(sf::Color(255, 0, 0));
                shape.setOutlineThickness(2.f);
                shape.setOutlineColor(sf::Color(250, 150, 100));
                shape.setPosition(rand() % (int)(window.getSize().x - shape.getRadius()*2), rand() % (int)(window.getSize().y - shape.getRadius()*2));
                window.draw(shape);
                window.display();
            } else {
                window.clear();

                window.display();
            }
            i += SMALLBUF;
            clock.restart();
        }
        if (circles.samples[0].empty()) {
            auto finish = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> elapsed = finish - start;
            std::cout << "Elapsed time: " << elapsed.count() << " s\n";
            return 0;
        }

    }
    return 0;
}

